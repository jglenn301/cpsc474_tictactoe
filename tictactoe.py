''' Finds optimal moves for Tic-Tac-Toe, assuming both players play an
    equilibrium strategy.  Functions in this module represent Tic-Tac-Toe
    positions using a square 2-D array (list of lists) of characters, each
    of which is 'X', 'O', or ' '
'''

# the starting position and direction for each of the possible
# three-in-a-rows
possible_wins = [((0, 0), (0, 1)),
                 ((0, 0), (1, 0)),
                 ((0, 0), (1, 1)),
                 ((0, 1), (1, 0)),
                 ((0, 2), (1, 0)),
                 ((0, 2), (1, -1)),
                 ((1, 0), (0, 1)),
                 ((2, 0), (0, 1))]


def terminal(s):
    ''' Determines if the given position is a terminal (game over) position.
        Returns true if so and false if not.

        s -- a Tic-Tac-Toe position, as a 2D array of 'X', 'O', and ' '
    '''
    # s is terminal if there are no moved left or someone has won
    return len(legal_moves(s)) == 0 or winner(s) is not None


def winner(s):
    ''' Determines who has won at the given position.  Returns 'X', 'O', or
        None.  None can mean no winner or a draw; use terminal to distinguish.

        s -- a reachable Tic-Tac-Toe position, as a 2D array of 'X', 'O', ' '
    '''
    # find the first three-in-a-row claimed by the same player
    return next((s[three[0][0]][three[0][1]] for three in possible_wins if wins_three(s, three[0], three[1]) is not None), None)


def in_bounds(s, r, c):
    ''' Determines if the given position is in the bounds of the game in
        the given state.

        s -- a Tic-Tac-Toe position, as a 2D array of 'X', 'O', and ' '
        r -- an integer
        c -- an integer
    '''
    return r >= 0 and r < len(s) and c >= 0 and c < len(s[r])


def wins_three(s, pos, delta):
    ''' Determines who has claimed all three positions in the three-in-a-row
        starting at the given position and going in the given direction.

        s -- a Tic-Tac-Toe position, as a 2D array of 'X', 'O', and ' '
        pos -- a position on the edge of the board
        delta -- a direction to an opposite edge, as a (delta row, delta col)
                 tuple with each component either 1, 0, or -1 but not both 0
    '''
    # unpack the tuple
    start_r = pos[0]
    start_c = pos[1]

    # has someone claimed the first space?
    if (s[start_r][start_c] == ' '):
        return None

    next_r = start_r + delta[0]
    next_c = start_c + delta[1]

    # search for a position not claimed by the same player as the first
    while in_bounds(s, next_r, next_c) and s[next_r][next_c] == s[start_r][start_c]:
        next_r += delta[0]
        next_c += delta[1]

    if in_bounds(s, next_r, next_c):
        # found such a position -- no win here
        return None
    else:
        # no such position -- player at the start has won
        return s[start_r][start_c]
    

def legal_moves(s):
    ''' Returns a list of the legal moves from position s.

        s -- a nonterminal Tic-Tac-Toe state represented as a 2D array
             of 'X', 'O', and ' '
    '''
    moves = []

    # find all ' ' in 2D array
    for r in range(len(s)):
        for c in range(len(s[r])):
            if s[r][c] == ' ':
                moves.append((r, c))
                
    return moves


def value(s, curr):
    ''' Returns the value of the given Tic-Tac-Toe position  (-1 if 'O' has
        a winning strategy, 1 if 'X' does, and 0 if neither does) and an
        optimal move if there is a winning or drawing move for the current
        player (None otherwise, including when the position is terminal). 
        The return value is a (value, move) tuple.

        s -- a reachable Tic-Tac-Toe state represented as a 2D array
             of 'X', 'O', and ' '
        curr - 'X' or 'O' for the current player (the player to move next)
    '''
    # base cases are terminal positions
    if terminal(s):
        w = winner(s)
        if w is None:
            return (0, None)
        elif w == 'X':
            return (1, None)
        else:
            return (-1, None)

    # get a list of the legal moves
    moves = legal_moves(s)
    
    if curr == 'X':
        # 'X' player tries to maximize value; find move that yields position
        # with maximum value
        s_value = -1
        best_move = None
        
        for move in moves:
            # update position to reflect move for current iteration
            s[move[0]][move[1]] = curr

            # recursively determine value of the resulting position
            next_value = value(s, 'O')[0]
            
            if next_value > s_value:
                # move led to a better state than best we'd seen so far
                s_value = next_value
                best_move = move

            # undo move so next iteration starts with original position
            s[move[0]][move[1]] = ' '
            
            if s_value == 1:
                # can't do better than +1; no need to check other moves
                return (s_value, best_move)
        return (s_value, best_move)
    else:
        # 'O' case is symmetric: switch 'X's and 'O's and min instead of max
        s_value = 1
        best_move = None
        for move in moves:
            s[move[0]][move[1]] = curr
            next_value = value(s, 'X')[0]
            if next_value < s_value:
                s_value = next_value
                best_move = move
            s[move[0]][move[1]] = ' '
            if s_value == -1:
                return (s_value, best_move)
        return (s_value, best_move)
        
    
print(value([[' ', ' ', ' '],
              [' ', ' ', ' '],
              [' ', ' ', ' ']], 'X'))
